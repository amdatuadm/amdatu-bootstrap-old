/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugin.api;

import java.util.Collection;
import java.util.Collections;

/**
 * Convenience base implementation of {@link Plugin} that stubs most of the methods with sensible defaults.
 */
public abstract class AbstractBasePlugin implements Plugin {
    
    @Override
    public Collection<Dependency> getDependencies() {
        return Collections.emptyList();
    }

    @Override
    public String getDescription() {
        return "";
    }

    @Override
    public Collection<Dependency> getRunRequirements() {
        return Collections.emptyList();
    }

    @Override
    public void install() throws Exception {
    }

    @Override
    public boolean isProjectInstallAllowed() {
        return true;
    }

    @Override
    public boolean isWorkspaceInstallAllowed() {
        return false;
    }

    @Override
    public String toString() {
        return getName();
    }
}
