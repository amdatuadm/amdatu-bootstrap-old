/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugin.api;

import java.util.Collection;

/**
 * Provides a piece of pluggable functionality that performs an action at a
 * workspace or project level. It can be as simple as adding or modifying files
 * in the project and/or workspace or interact with the user to provide a more
 * complex piece of functionality.<br/>
 * Plugins are supposed to be able to detect whether or not they are already
 * "installed" in a workspace and/or project, for example by inspecting the
 * contents of files.
 */
public interface Plugin {
    /**
     * The name of this plugin, used to identify this plugin in the system and
     * provide a scope for the functionality provided by this plugin.
     * 
     * @return the name of this plugin, can not be <code>null</code>.
     */
    String getName();

    /**
     * Provides a human-readable description of what this plugin does.
     * 
     * @return a description, can be <code>null</code> if no description is
     *         desired.
     */
    String getDescription();

    /**
     * Returns the compile-time dependencies this plugin needs or wants to
     * install. This information is used to adapt the project configuration in
     * order to make the project or workspace in which this plugin is installed
     * compile against the correct dependencies.
     * 
     * @return a collection with the dependencies of this plugin, never
     *         <code>null</code>, but can be empty.
     * @see Dependency
     */
    Collection<Dependency> getDependencies();

    /**
     * Returns the runtime dependencies this plugin needs or wants to install.
     * This information is used to provide the correct dependencies for running
     * a project in which this plugin is installed.
     * 
     * @return a collection with the runtime dependencies, never
     *         <code>null</code>, but can be empty.
     * @see Dependency
     */
    Collection<Dependency> getRunRequirements();

    /**
     * Invokes this plugin to "install" it in the destination workspace and/or
     * project.
     * 
     * @throws Exception
     *         in case the installation of this plugin failed.
     */
    void install() throws Exception;

    /**
     * Returns whether or not this plugin is already installed in the
     * destination workspace and/or project.
     * 
     * @return <code>true</code> if this plugin is already installed,
     *         <code>false</code> otherwise.
     */
    boolean isInstalled();

    /**
     * Indicates that this plugin can be installed at a workspace level.
     * 
     * @return <code>true</code> if this plugin can function at workspace level,
     *         <code>false</code> (the default) otherwise.
     */
    boolean isWorkspaceInstallAllowed();

    /**
     * Indicates that this plugin can be installed at a project level.
     * 
     * @return <code>true</code> (the default) if this plugin can function at project level,
     *         <code>false</code> otherwise.
     */
    boolean isProjectInstallAllowed();
}
