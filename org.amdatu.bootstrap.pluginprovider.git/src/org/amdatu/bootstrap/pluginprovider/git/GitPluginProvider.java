/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.pluginprovider.git;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.bootstrap.plugins.gradle.api.GradlePlugin;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.eclipse.jgit.api.Git;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;

import aQute.bnd.build.Project;

@Component(provides = Plugin.class)
public class GitPluginProvider extends AbstractBasePlugin {

	@ServiceDependency
	private volatile Prompt m_prompt;

	@ServiceDependency
	private volatile GradlePlugin m_gradlePlugin;

	@ServiceDependency
	private volatile Navigator m_navigator;

	private volatile BundleContext m_bundleContext;

	@Command
	public void installFromUrl() throws Exception {
		String repo = m_prompt.askString("What is the GIT repository url of the plugin?");
		Path buildDir = Files.createTempDirectory(null);

		Git.cloneRepository().setURI(repo).setDirectory(buildDir.toFile()).call();

		m_navigator.changeDir(buildDir);

		m_gradlePlugin.build();
		List<File> plugins = new ArrayList<>();

		for (Project project : m_navigator.getCurrentWorkspace().getBuildOrder()) {
			File[] buildFiles = project.getBuildFiles();
			for (File file : buildFiles) {
				plugins.add(file);
			}
		}

		File pluginToInstall = m_prompt.askChoice("Which jar do you want to install?", 0, plugins);
		Files.copy(pluginToInstall.toPath(), m_bundleContext.getDataFile(pluginToInstall.getName()).toPath(),
				StandardCopyOption.REPLACE_EXISTING);
		Bundle installBundle = m_bundleContext.installBundle(m_bundleContext.getDataFile(pluginToInstall.getName()).toURI().toString());
		installBundle.start();
		System.out.println("Plugin " + installBundle.getSymbolicName() + " successfully installed");
	}

	@Override
	public String getName() {
		return "gitplugin";
	}

	 /***************************************************
     * This plugin can't be installed as its a provider *
     ****************************************************/

    @Override
    public boolean isProjectInstallAllowed() {
        return false;
    }

    @Override
    public boolean isWorkspaceInstallAllowed() {
        return false;
    }

    @Override
    public boolean isInstalled() {
        return true;
    }

}
