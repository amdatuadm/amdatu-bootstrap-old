/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.sourcecode;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.Date;

import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateException;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

@Component(provides = Plugin.class)
public class HeadersPlugin extends AbstractBasePlugin {
	@ServiceDependency(required = true)
	private volatile TemplateEngine m_templateEngine;

	@ServiceDependency
	private volatile Prompt m_prompt;

	@ServiceDependency
	private volatile Navigator m_navigator;

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;
	
	private volatile BundleContext m_bundleContext;

	@Command
	public void addHeader(File source) throws IOException, TemplateException {
		Date now = new Date();
		URL templateUri = m_bundleContext.getBundle().getEntry("/templates/amdatu.vm");
		TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
		TemplateContext context = m_templateEngine.createContext();
		context.put("copyrightYears", 1900 + now.getYear());
		String template = processor.generateString(context);
		
		File sourceFile = m_navigator.getCurrentDir().resolve(source.toString()).toFile();
		File targetFile = new File(sourceFile.getParentFile(), sourceFile.getName() + ".new");
		BufferedReader br = null;
		BufferedWriter bw = null;
		try {
			br = new BufferedReader(new FileReader(sourceFile));
			bw = new BufferedWriter(new FileWriter(targetFile));
			bw.write(template);
			bw.newLine();
			String line = br.readLine();
			if (line.equals("/*")) {
				targetFile.delete();
				throw new IOException("Source file probably already contains a header (first line is '/*').");
			}
			while (line != null) {
				bw.write(line);
				bw.newLine();
				line = br.readLine();
			}
			bw.close();
			br.close();
			bw = null;
			br = null;
			if (sourceFile.delete()) {
				targetFile.renameTo(sourceFile);
			}
			else {
				targetFile.delete();
				throw new IOException("Could not delete source file (to replace it with an update with the header).");
			}
		}
		finally {
			if (bw != null) {
				try {
					bw.close();
				}
				finally {
					if (br != null) {
						br.close();
					}
				}
			}
		}
	}

	@Override
	public String getName() {
		return "Headers";
	}

	@Override
	public boolean isInstalled() {
		return true;
	}
}
