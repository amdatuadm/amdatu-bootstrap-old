/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.console.impl;

import java.util.List;
import java.util.Scanner;

import org.amdatu.bootstrap.core.api.FullyQualifiedName;
import org.amdatu.bootstrap.core.api.Prompt;
import org.apache.felix.gogo.api.CommandSessionListener;
import org.apache.felix.service.command.CommandSession;

public class ConsolePrompt implements Prompt, CommandSessionListener{
	private volatile CommandSession m_session;
	

	@Override
	public boolean askBoolean(String message, boolean defaultChoice) {
		if(defaultChoice) {
			System.out.print(message + ": [Y/n]");
		} else {
			System.out.print(message + ": [y/N]");
		}
		
		//Can't close scanner here, or Gogo will crash
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(m_session.getKeyboard());
		
		String answer = scanner.nextLine();
		if(answer != null && answer.length() > 0) {
			return answer.equalsIgnoreCase("y");
		} else {
			return defaultChoice;
		}
	}

	@Override
	public void afterExecute(CommandSession arg0, CharSequence arg1, Exception arg2) {
		m_session = null;
	}

	@Override
	public void afterExecute(CommandSession arg0, CharSequence arg1, Object arg2) {
		m_session = null;
	}

	@Override
	public void beforeExecute(CommandSession session, CharSequence arg1) {
		m_session = session;
	}

	@Override
	public String askString(String message) {
		return askString(message, null);
	}

	@Override
	public String askString(String message, String defaultString) {
		if(defaultString != null) {
			System.out.print(message + ": [" + defaultString +"]");
		} else {
			System.out.print(message + ": ");
		}
		
		//Can't close scanner here, or Gogo will crash
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(m_session.getKeyboard());
		
		String answer = scanner.nextLine();
		if(answer != null && answer.length() > 0) {
			return answer;
		} else if(defaultString != null) {
			return defaultString;
		} else {
			return askString(message);
		}
	}


	@Override
	public <T> T askChoice(String message, int defaultOption, List<T> options) {
		int askChoiceAsIndex = askChoiceAsIndex(message, defaultOption, options);
		if (askChoiceAsIndex >= 0) {
		    return options.get(askChoiceAsIndex);
		}
		// not a valid option
		return null;
	}
	
	@Override
	public int askChoiceAsIndex(String message, int defaultOption, List<? extends Object> options) {
		if(options.size() == 0) {
			throw new IllegalArgumentException("No options specified");
		}
		
		System.out.println(message + ":");
		
		for(int i=0; i < options.size(); i++) {
			if(defaultOption == i) {
				System.out.println(String.format("[%d]* - %s", i, options.get(i)));
			} else {
				System.out.println(String.format("[%d] - %s", i, options.get(i)));
			}
		}
		
		//Can't close scanner here, or Gogo will crash
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(m_session.getKeyboard());
		
		String answer = scanner.nextLine();
		try {
			int answerInt = Integer.parseInt(answer);
			if(answerInt < options.size()) {
				return answerInt;
			}
		} catch(NumberFormatException e) {
			//Recover by returning default option
		}
		
		return defaultOption;
	}

	@Override
	public FullyQualifiedName askComponentName() {
		return askComponentName("What should be the name of the component?", "example.Example");
	}

	@Override
	public FullyQualifiedName askComponentName(String message) {
		return askComponentName(message, "example.Example");
	}

	@Override
	public FullyQualifiedName askComponentName(String message, String defaultName) {
		String name = askString(message, defaultName);
		
		return new FullyQualifiedName(name);
	}
}
