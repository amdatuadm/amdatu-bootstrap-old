/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.api;

import java.util.List;

public interface Prompt {
	boolean askBoolean(String message, boolean defaultChoice);
	String askString(String message);
	String askString(String message, String defaultString);
	<T> T askChoice(String message, int defaultOption, List<T> options);
	int askChoiceAsIndex(String message, int defaultOption, List<? extends Object> options);
	
	FullyQualifiedName askComponentName();
	FullyQualifiedName askComponentName(String message);
	FullyQualifiedName askComponentName(String message, String defaultName);
	
}
