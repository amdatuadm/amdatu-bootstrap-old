/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.api;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import aQute.bnd.build.Project;
import aQute.bnd.build.Workspace;

public interface Navigator {

	Workspace getCurrentWorkspace();

	Project getCurrentProject();

	Path getCurrentDir();

	void changeDir(Path newDir);

	Path getProjectDir();

	Path getWorkspaceDir();

	void createProject(String name);

	Path getBndFile();
	
    List<Path> getBndRunFiles();
	
	List<File> listProjectBndFiles(); 
	
	List<Path> findWorkspaceRunConfigs();

    void createWorkspace() throws IOException;

    String getProjectName();

	Path getHomeDir();
	
	Path getPreviousDir();

}
