/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core;

import org.amdatu.bootstrap.plugin.api.Plugin;
import org.apache.felix.dm.Component;

public class PluginRegistration {
	private final Plugin m_plugin;
	private final Component m_command;
	
	public PluginRegistration(Plugin plugin, Component command) {
		m_plugin = plugin;
		m_command = command;
	}

	public Plugin getPlugin() {
		return m_plugin;
	}

	public Component getCommand() {
		return m_command;
	}
}
