Amdatu Bootstrap
==========

Amdatu Bootstrap is the easiest way to get started with OSGi development and Bndtools. It is a command line tool that helps you to setup Bndtools projects. Instead of Googling and copy-pasting your way to a new project, this tool sets up projects interactively. 

Some of the things Amdatu Bootstrap can do:

* Add dependencies for often used libraries
* Setup run configuration for often used libraries
* Create template files to supercharge development
* Create your own plugins

[See it in action!](https://vimeo.com/user17212182/review/83262745/5a2978544c)

Getting started
==========
[Download](https://bitbucket.org/amdatu/amdatu-bootstrap/downloads/amdatu-bootstrap.zip) Amdatu Boostrap.

Unzip the archive and start Amdatu Bootstrap.
    
    cd amdatu-bootstrap
    java -jar bin/felix.jar

Navigate to an existing workspace

	g! cd some-existing-workspace

List available plugins

	g! listplugins

Show help for a plugin

	g! man core

Create a new project

	g! createproject
	Name of the project?: example

This created an empty bnd project which can be imported in Bndtools. Now let's add some capabilities.

	g! install
	Which plugin do you want to install?:
	[0]* - angular
	[1] - dependencymanager
	[2] - rest

	1

	Added Dependency [bsn=org.apache.felix.dependencymanager, version=null]
	Added Dependency [bsn=osgi.core, version=null]
	Do you want to use annotations?: [Y/n]

	Y

Installing the DependencyManager plugin added Apache Felix Dependency Manager to the project's build path. Optionally it can also install it's annotation processor in the workspace to use annotations. View the bnd file to see the added dependencies.

Now let's create an Amdatu REST component.

	g! install rest
	Added Dependency [bsn=org.amdatu.web.jaxrs, version=null]
	Added Dependency [bsn=org.amdatu.web.rest.doc, version=null]
	Added Dependency [bsn=javax.servlet, version=null]

	g! rest:addcomponent
	How do you want to register the component?:
	[0]* - Annotations
	[1] - Activator

	0

	What should be the name of the component?: [example.Example]

	On what path do you want to register the component?: 

	example

	Created file: /Users/paul/tmp/j1demo/example/src/example/Example.java

A new file Example.java is added to the project with an example REST component.


Q&A 
==========
**Why an extra tool, can't Bndtools do this?**

Bndtools is awesome, but extending it is not trivial. Because Bndtools is an Eclipse plugin, you need quite some Eclipse knowledge to be able to extend it. In contrast, Amdatu Bootstrap is designed to be easily extensible, so it's trivial to add new plugins. 

**Can I use Amdatu Bootstrap with Maven?**

No. Amdatu Bootstrap is built on top of bnd, and assumes bnd's workspace structure and configuration.

**How does Amdatu Bootstrap compares to JBoss Forge?**

Amdatu Bootstrap is inspired by JBoss Forge. Forge is awesome and has a lot more features than Amdatu Bootstrap. It is built on top of Maven and works best for Maven-like projects however. Adapting Forge to support bnd projects would be possible, but would be far from ideal. Also, it would require plugin developers to step out of the familiar OSGi devepment environment. 


Writing your own plugin
==========

Amdatu Bootstrap is designed to be extensible, creating a plugin is trivial. First we have some setup to do.

* Clone the Amdatu Bootstrap repository
* Create a Bndtools workspace for the cloned repo
* Check if you can run Amdatu Bootstrap from within Eclipse. Start run/console.bndrun.

Now add a new Bndtools OSGi project and setup the following buildpath:

	-buildpath: \
	   org.amdatu.bootstrap.plugin.api;version=latest,\
	   org.amdatu.bootstrap.core;version=latest,\
	   ../cnf/plugins/org.apache.felix.dependencymanager.annotation-3.1.1-SNAPSHOT.jar;version=file,\
	   osgi.core
	
Create a new class that represents your plugin. Basically a plugin is just an OSGi service registered as *Plugin*. Most plugins extend the *AbstractBasePlugin* class. The Amdatu Bootstrap runtime will register each *@Command* method as a command. 

	import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
	import org.amdatu.bootstrap.plugin.api.Command;
	import org.amdatu.bootstrap.plugin.api.Plugin;
	import org.apache.felix.dm.annotation.api.Component;

	@Component(provides=Plugin.class)
	public class Example extends AbstractBasePlugin{

		@Command
		public void hello() {
			System.out.println("Hello");
		}
		
		@Override
		public String getName() {
			return "example";
		}

		@Override
		public boolean isInstalled() {
			return false;
		}

	}


Make sure to add the package to the 'Private Packages' in the bnd file.
Now add the plugin to the run configuration in 'run/console.bndrun'.

You should now be able to execute the 'hello' command.

Adding dependencies
----------
Many plugins setup the build path of a project. The easiest way to provide new dependencies to install is to override the 'getDependencies' method. Dependencies returned by this method will automatically be installed when the *install* command is used.

    @Override
	public Collection<Dependency> getDependencies() {
		return Dependency.fromStrings("my.first.dependency", "my.second.dependency");
	}

Providing run configuration
-------
Similar to setting up the build path, it is also common for plugins to provide to setting up a run configuration. For example, to setup Jetty and the Apache Felix  HTTP Whiteboard you could provide the following:

    @Override
	public Collection<Dependency> getRunRequirements() {
		return Dependency.fromStrings(
				"org.apache.felix.http.jetty", 
				"org.apache.felix.http.whiteboard");
	}


Custom setup
---------
Some plugins require other forms of setup besides providing dependencies. For example, a plugin for JQuery could install the JQuery library to the project.

	@Override
	public void install() throws Exception {
		new DirectoryStructureBuilder(m_navigator.getProjectDir(), "static/js/lib");
		
		URL angularFile = m_bundleContext.getBundle().getEntry("/libs/js/jquery.js");
		
		try(InputStream in = angularFile.openStream()) {
			Files.copy(in, m_navigator.getProjectDir().resolve("static/js/lib/jquery.js"), StandardCopyOption.REPLACE_EXISTING);
		}
	}

Prompting the user
-----------
Plugins can interact with the user, they are not static scripts. To interact with the user the *Prompt* interface can be used. Because plugins are just OSGi services, we can use depenency injection to access Amdatu Bootstrap services.

    @ServiceDependency
	private volatile Prompt m_prompt;

    @Command
	public void sayHello() {
		String name = m_prompt.askString("What is your name?", "Stranger");
		String language = m_prompt.askChoice("What is your favorite language?", 0, 
				Arrays.asList("Java", "JavaScript", "Groovy", "Scala"));
		
		System.out.println("Hello " + name + ", happy to meet another " + language + " programmer");
	}

Getting information about the project
-----------
Amdatu Bootstrap understands bnd workspaces. The *Navigator* service is responsible for keeping information about the workspace and the current project. Simply inject the service into your plugin.

    @ServiceDependency
	private volatile Navigator m_navigator;

The *Navigator* provides several useful methods, including the following:

    Workspace getCurrentWorkspace();
	Project getCurrentProject();
	Path getCurrentDir();
	Path getProjectDir();
	Path getWorkspaceDir();
	Path getBndFile();
	List<File> listProjectBndFiles(); 


Providing services to other plugins
-----------
Besides adding commands that are used directly by the end user, a plugin can also provide services to other plugins. Simply register an extra interface to the service registry. 

For example, the built-in Dependency Manager plugin provides the following interface.

	public interface DependencyManagerPlugin {
		void createActivator(FullyQualifiedName fqn, String interfaceName);
	}


Creating files
--------
Plugins can create files as a starting point for development. Amdatu Templates can be used to easily create templates for files. For example we can create a new Java class:
	private volatile BundleContext m_bundleContext;
	
	@ServiceDependency
	private TemplateEngine m_templateEngine;
	
    @Command
	public void addComponent() {
		FullyQualifiedName fqn = m_prompt.askComponentName();
		
		URL templateUri = m_bundleContext.getBundle().getEntry("/templates/restcomponent.vm");
		
		try {
			TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
			TemplateContext context = m_templateEngine.createContext();
			context.put("componentName", fqn.getClassName());
			context.put("packageName", fqn.getPackageName());
			
			String template = processor.generateString(context);
			Path outputDir = m_navigator.getCurrentDir().resolve("src").resolve(fqn.getPackageName().replaceAll("\\.", "\\"));
			Path outputFile = outputDir.resolve(fqn.getClassName() + ".java");
			Files.createDirectories(outputDir);
			
			System.out.println("Created file: " + outputFile);
			Files.write(outputFile, template.getBytes());
			
		} catch (TemplateException | IOException e) {
			throw new RuntimeException(e);
		}
	}


Creating a release
-------
During plugin development you should run Amdatu Bootstrap from Bndtools for a fast development turn around. You can also easily create a binary release however. From the root of the workspace type:

    gradle release

This generates a folder 'release/felix-framework-4.2.1'. Start the framework by starting Apache Felix.

    cd release/felix-framework-4.2.1
    java -jar bin/felix.jar


How do I...
---------
Take a look at the built-in plugins for more examples! :-)

