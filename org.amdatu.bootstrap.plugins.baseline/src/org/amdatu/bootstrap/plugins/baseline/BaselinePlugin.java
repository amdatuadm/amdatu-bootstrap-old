/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.baseline;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;

import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.bootstrap.plugins.gradle.api.GradlePlugin;
import org.amdatu.bootstrap.plugins.version.api.VersionService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

import aQute.bnd.build.Project;
import aQute.bnd.build.ProjectBuilder;
import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.differ.Baseline;
import aQute.bnd.differ.Baseline.Info;
import aQute.bnd.differ.DiffPluginImpl;
import aQute.bnd.osgi.Constants;
import aQute.bnd.osgi.Jar;
import aQute.bnd.osgi.Processor;
import aQute.bnd.properties.Document;
import aQute.bnd.version.Version;

@Component(provides = Plugin.class)
public class BaselinePlugin extends AbstractBasePlugin {

    @ServiceDependency
    private volatile Navigator m_navigator;
    
    @ServiceDependency
    private volatile Prompt m_prompt;

    @ServiceDependency
    private volatile ResourceManager m_resourceManager;

    @ServiceDependency
    private volatile VersionService m_versionService;

    @ServiceDependency
    private volatile GradlePlugin m_gradle;
    
    @Override
    public String getName() {
        return "baseline";
    }
    
    @Command(description = "Automate baselining using gradle as a build agent")
    public boolean autoBaseline() throws Exception {
        Path currentWorkspace = m_navigator.getWorkspaceDir();
        if (currentWorkspace == null) {
            throw new IllegalStateException("Need to be in a workspace to do this");
        }
        if (!m_navigator.getWorkspaceDir().resolve("build.gradle").toFile().exists()) {
            throw new IllegalStateException("Gradle build file not found can't auto baseline, use the normal baseline instead.");
        }
        for (int i = 1 ; i <= 10; i++) {
            m_gradle.build();
            if (baseline(true)) {
                System.out.println("All baseline errors fixed in " + i + " iterations");
                return true;
            }
        }
        System.out.println("Baseline errors not fixed withing 10 iterations");
        return false;
    }
    
    @Command(description = "Baseline all generated jars in this workspace")
    public boolean baseline() throws Exception {
        return baseline(false);
    }
    
    private boolean baseline(boolean autoFix) throws Exception {
        Path currentWorkspace = m_navigator.getWorkspaceDir();
        if (currentWorkspace == null) {
            throw new IllegalStateException("Need to be in a workspace to do this");
        }
        // init repo's
        m_versionService.getRepositories();
        DiffPluginImpl differ = new DiffPluginImpl();
        Map<String, List<BaseLineError>> baseLineErrors = new HashMap<>();
        List<File> generatedJars = getGeneratedJars();
        for (File jarFile : generatedJars) {
            try (Jar jar = new Jar(jarFile)) {
                String bsn = jar.getBsn();
                System.out.println("Baselining: " + bsn);
                SortedSet<Version> findVersion = m_versionService.findVersion(bsn);
                if (!findVersion.isEmpty()) {
                    File repoFile = m_versionService.downloadVersion(bsn, findVersion.last().toString());
                    if (repoFile == null) {
                        System.err.println("Failed to get " + bsn + " with version " + findVersion.last()
                            + " skipping baseline of this file!");
                    }
                    else {
                        Jar baseLineJar = new Jar(repoFile);
                        Processor bnd = new Processor();
                        Baseline baseLine = new Baseline(bnd, differ);
                        baseLine.baseline(jar, baseLineJar, null);
                        List<BaseLineError> errors = new ArrayList<>();
                        if (baseLine.getBundleInfo().mismatch) {
                            BaseLineError err = new BaseLineError();
                            err.bsn = bsn;
                            err.newVersion = baseLine.getSuggestedVersion().toString();
                            errors.add(err);
                        } 
                        for (Info i : baseLine.getPackageInfos()) {
                            if (i.mismatch) {
                                BaseLineError err = new BaseLineError();
                                err.bsn = bsn;
                                err.packageName = i.packageName;
                                err.newVersion = baseLine.getSuggestedVersion().toString();
                                errors.add(err);
                            }
                        }
                        if (!errors.isEmpty()) {
                            baseLineErrors.put(bsn, errors);
                        }
                    }
                }
            }
        }
        if (!baseLineErrors.isEmpty()) {
            System.out.println("Baselinine erros:");
            for (Entry<String, List<BaseLineError>> entry : baseLineErrors.entrySet()) {
                System.out.println(entry.getKey() + ": ");
                for (BaseLineError e : entry.getValue()) {
                    System.out.println("  " + e.toString());
                }
            }
            if (autoFix || m_prompt.askBoolean("Do you want to fix them now?", true)){
                fixBaseLineErrors(baseLineErrors);
            }
            return false;
        } else {
            System.out.println("Baseline OK.");
            return true;
        }
    }

    private void fixBaseLineErrors(Map<String, List<BaseLineError>> baseLineErrors) throws Exception {
        List<File> generatedJars = getGeneratedJars();
        for (File jarFile : generatedJars) {
            try (Jar jar = new Jar(jarFile)) {
                String bsn = jar.getBsn();
                if (baseLineErrors.containsKey(bsn)) {
                    for (BaseLineError err : baseLineErrors.get(bsn)) {
                        if (err.packageName == null) {
                            fixProject(jarFile, bsn, err);
                        } else {
                            fixPackage(jarFile, bsn, err);
                        }
                    }
                }
            }
        }
        System.out.println("All done, you might want to trigger a new build to update the generated files.");
    }

    private void fixPackage(File jarFile, String bsn, BaseLineError err) throws IOException {
        File projectFolder = jarFile.getParentFile().getParentFile();
        List<File> packageFiles = m_resourceManager.findFiles(projectFolder, "packageinfo");
        for (File f : packageFiles) {
            if (f.getAbsolutePath().replace(File.separatorChar, '.').contains(err.packageName)) {
                try (PrintWriter out = new PrintWriter(f)){
                    out.print("version " + err.newVersion + "\n");
                }
            }
        }
    }

    private void fixProject(File jarFile, String bsn, BaseLineError err) {
        File projectFolder = jarFile.getParentFile().getParentFile();
        try {
            Project project = m_navigator.getCurrentWorkspace().getProject(projectFolder.getName());
            
            File bndFile = null;
            if (project.get(Constants.SUB) == null && project.getBuilder(null).getBsn().equals(bsn)){
            	bndFile = project.getPropertiesFile();
            }else{
            	ProjectBuilder subBuilder = project.getSubBuilder(bsn);
            	bndFile = subBuilder.getPropertiesFile();
            }

            BndEditModel model = new BndEditModel();
            model.loadFrom(bndFile);
   
            model.setBundleVersion(err.newVersion);
   
            Document document = new Document(new String(Files.readAllBytes(bndFile.toPath())));
            model.saveChangesTo(document);
   
            String documentContents = document.get();
            m_resourceManager.writeFile(bndFile.toPath(), documentContents.getBytes());
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
            
    private List<File> getGeneratedJars() throws Exception {
        List<File> result = new ArrayList<>();
        
        Project project = m_navigator.getCurrentProject();
        if (project != null) {
            result.addAll(addGeneratedJarsFromProject(project));
        } else {
            Collection<Project> projects = m_navigator.getCurrentWorkspace().getAllProjects();
			for (Project p : projects) {
				result.addAll(addGeneratedJarsFromProject(p));
			}
        }
        return result;
    }

    private List<File> addGeneratedJarsFromProject(Project project) {
    	File genFolder = project.getTargetDir();
        
        if (genFolder.exists() && genFolder.isDirectory()) {
            File[] jars = genFolder.listFiles(new FileFilter() {
                @Override
                public boolean accept(File pathname) {
                    return pathname.getName().endsWith(".jar");
                }
            });
            return Arrays.asList(jars);
        }
        return Collections.emptyList();
    }
    
    /*******************************************************************
     * This plugin can't be installed as not something you can install *
     *******************************************************************/

    @Override
    public boolean isProjectInstallAllowed() {
        return false;
    }

    @Override
    public boolean isWorkspaceInstallAllowed() {
        return false;
    }

    @Override
    public boolean isInstalled() {
        return true;
    }

    /** A data holder class for the baseline errors. */
    private class BaseLineError {
        String bsn;
        String packageName;
        String newVersion;
        
        @Override
        public String toString() {
            if (packageName != null) {
                return "In " + bsn + " package: " + packageName + " should be " + newVersion;
            }
            return bsn + " should be " + newVersion;
        }
    }
   
}
