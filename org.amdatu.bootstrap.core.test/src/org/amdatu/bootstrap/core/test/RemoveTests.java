package org.amdatu.bootstrap.core.test;

import java.io.IOException;
import java.nio.file.Path;

import org.amdatu.bootstrap.testframework.AbstractBaseTestCase;

public class RemoveTests extends AbstractBaseTestCase{

	public void testRmFile() throws Exception {
		Path path = m_navigator.getCurrentDir().resolve("newfile.txt");
		path.toFile().createNewFile();
		
		assertTrue(path.toFile().exists());
		
		runScript("rm newfile.txt");
		
		assertFalse(path.toFile().exists());
	}
	
	public void testRmEmptyDir() throws Exception {
		Path path = m_navigator.getCurrentDir().resolve("newdir");
		path.toFile().mkdir();
		
		assertTrue(path.toFile().exists());
		assertTrue(path.toFile().isDirectory());
		
		runScript("rm newdir");
		
		assertFalse(path.toFile().exists());
	}
	
	public void testRmNonEmptyDir() throws Exception {
		Path path = m_navigator.getCurrentDir().resolve("newdir");
		path.toFile().mkdir();
		
		Path filePath = path.resolve("newfile.txt");
		filePath.toFile().createNewFile();
		
		assertTrue(filePath.toFile().exists());
		
		runScript("rm newdir");
		
		assertFalse(path.toFile().exists());
		assertFalse(filePath.toFile().exists());
	}
}
