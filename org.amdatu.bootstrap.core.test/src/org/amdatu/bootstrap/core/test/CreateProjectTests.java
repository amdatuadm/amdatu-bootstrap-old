/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.test;

import java.nio.file.Paths;

import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.testframework.AbstractBaseTestCase;

public class CreateProjectTests extends AbstractBaseTestCase {

	@Override
	protected void setUp() throws Exception {
		addServiceDependencies(Navigator.class);
		super.setUp();
		
		runScript("createproject myproject");
	}
	
	public void testCreateProject() throws Exception {
		boolean exists = Paths.get("build/ws/testWorkspace/myproject/bnd.bnd").toFile().exists();
		assertTrue("project bnd file should be created", exists);
	}
	
	public void testCdToProjectAfterCreation() throws Exception {
		assertEquals(Paths.get("myproject"), m_navigator.getCurrentDir().getFileName());
	}
}
