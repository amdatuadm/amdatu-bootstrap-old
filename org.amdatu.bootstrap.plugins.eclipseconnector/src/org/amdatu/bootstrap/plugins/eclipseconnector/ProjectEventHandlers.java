/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.eclipseconnector;

import org.amdatu.bootstrap.plugins.eclipseconnector.api.EclipseConnector;
import org.osgi.service.event.Event;
import org.osgi.service.event.EventHandler;

public class ProjectEventHandlers implements EventHandler{
    
    private volatile EclipseConnector m_eclipseConnector;
    
	@Override
	public void handleEvent(Event event) {
		switch(event.getTopic()) {
			case "org/amdatu/bootstrap/core/PROJECT_CREATED": 
		        m_eclipseConnector.importProject((String) event.getProperty("projectname"));
	        break;
			case "org/amdatu/bootstrap/core/PROJECT_UPDATED":
			    m_eclipseConnector.refreshProject((String) event.getProperty("projectname"));
			break;
		}
	}

}
