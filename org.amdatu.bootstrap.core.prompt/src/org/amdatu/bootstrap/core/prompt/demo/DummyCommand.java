package org.amdatu.bootstrap.core.prompt.demo;

import java.util.HashMap;
import java.util.Map;

import org.amdatu.bootstrap.core.prompt.console.ConsolePrompt;
import org.amdatu.bootstrap.core.prompt.console.FQN;

import aQute.bnd.annotation.metatype.Meta;

public class DummyCommand {
	///// TEST CODE
	
	@Meta.OCD interface NameAndType {
		enum TYPE { CLASS, INTERFACE };
		@Meta.AD(name="FQN", description="The fully qualified name of the class.") FQN name();
		@Meta.AD(description="The type (class or interface)") TYPE type();
	}
	@Meta.OCD interface Extends {
		@Meta.AD(description="Class to extend", deflt="java.lang.Object") String extendName();
	}
	
//	@Command({NameAndType.class, Other.class})
//	public void bla() {
//		NamedAndTyped bla = prompt.ask(NameAndType.class);
//	}
//	
//	@Command(types={NameAndType.class, Extends.class})
	public static void main(String[] args) {
		ConsolePrompt p = new ConsolePrompt();
		Map<String, Object> values = new HashMap<>();
//		values.put("name", "Foo");
		NameAndType nat = p.ask(NameAndType.class, values);
		Extends ext = null;
		if (nat.type() == NameAndType.TYPE.CLASS) {
			ext = p.ask(Extends.class, values);
		}
		System.out.println("N: " + nat.name());
		System.out.println("T: " + nat.type());
		System.out.println("E: " + (ext == null ? "-" : ext.extendName()));
		
	}

}
