package org.amdatu.bootstrap.core.prompt;

import java.util.Map;

public interface Prompt {
	/**
	 * Ask a set of questions.
	 * You can provide answers upfront.
	 * 
	 * @param questions Questions, encoded in a metatype interface.
	 * @param answers Answers, encoded in a map.
	 * @return
	 */
	public <T> T ask(Class<T> questions, Map<String, Object> answers);
}
