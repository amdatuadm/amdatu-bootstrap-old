/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.gradle;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.PosixFilePermission;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.amdatu.bootstrap.core.api.DependencyBuilder;
import org.amdatu.bootstrap.core.api.Navigator;
import org.amdatu.bootstrap.core.api.Prompt;
import org.amdatu.bootstrap.core.api.ResourceManager;
import org.amdatu.bootstrap.plugin.api.AbstractBasePlugin;
import org.amdatu.bootstrap.plugin.api.Command;
import org.amdatu.bootstrap.plugin.api.Plugin;
import org.amdatu.bootstrap.plugins.gradle.api.GradlePlugin;
import org.amdatu.template.processor.TemplateEngine;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

@Component(provides = { Plugin.class, GradlePlugin.class })
public class GradlePluginImpl extends AbstractBasePlugin implements GradlePlugin {
    private final static List<String> NOT_FINAL_VERSIONS = Arrays.asList("milestone", "rc");
    
    private static String OS = System.getProperty("os.name").toLowerCase();

    @ServiceDependency
    private volatile DependencyBuilder m_dependencyBuilder;

    @ServiceDependency
    private volatile Navigator m_navigator;

    @ServiceDependency
    private volatile Prompt m_prompt;

    @ServiceDependency
    private volatile ResourceManager m_resourceManager;

    @ServiceDependency
    private volatile TemplateEngine m_templateEngine;

    private volatile BundleContext m_bundleContext;

    @Override
    public void install() throws IOException {
        Path workspaceDir = m_navigator.getWorkspaceDir();
        if (workspaceDir == null) {
            System.out.println("Need to be in workspace to install gradle");
            return;
        }
        List<String> gradleVersions = getGradleVersions();
        String versionToInstall =
            m_prompt.askChoice("What version do you want to install?", gradleVersions.size() - 1, gradleVersions);
        
        System.out.println("Downloading gradle version " + versionToInstall);
        String toFile = System.getProperty("java.io.tmpdir") + "/gradle-" + versionToInstall + ".zip";
        downloadFile("http://services.gradle.org/distributions/gradle-" + versionToInstall + "-bin.zip", toFile);
        
        System.out.println("Download complete, now extracting...");
        String toDir = System.getProperty("java.io.tmpdir");
        m_resourceManager.unzipFile(new File(toFile), new File(toDir));

        String gradleFolder = toDir + "/gradle-" + versionToInstall;
        setGradleExecutePermissions(gradleFolder);
        startProcess(new File(gradleFolder).toPath(), "./bin/gradle", "wrapper");

        copyWrapperToWorkspace(gradleFolder);
        
        copyBuildFile();

        // Now remove the temp file
        new File(toFile).delete();
        m_resourceManager.delete(new File(gradleFolder));
    }
    
    /**
     * Wrapper files:
     * gradlew
     * gradlew.bat
     * gradle/wrapper/
     *   gradle-wrapper.jar
     *   gradle-wrapper.properties
     * 
     * @param gradleFolder the folder where gradle is extracted
     * @throws IOException 
     */
    private void copyWrapperToWorkspace(String gradleFolder) throws IOException {
        String workspaceDir = m_navigator.getWorkspaceDir().toString();
        Files.copy(new File(gradleFolder+"/gradlew").toPath(), new File(workspaceDir + "/gradlew").toPath(), StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        Files.copy(new File(gradleFolder+"/gradlew.bat").toPath(), new File(workspaceDir + "/gradlew.bat").toPath(), StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        // create the wrapper folder
        new File(workspaceDir + "/gradle/wrapper/").mkdirs();
        Files.copy(new File(gradleFolder+"/gradle/wrapper/gradle-wrapper.jar").toPath(), new File(workspaceDir + "/gradle/wrapper/gradle-wrapper.jar").toPath(), StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
        Files.copy(new File(gradleFolder+"/gradle/wrapper/gradle-wrapper.properties").toPath(), new File(workspaceDir + "/gradle/wrapper/gradle-wrapper.properties").toPath(), StandardCopyOption.COPY_ATTRIBUTES, StandardCopyOption.REPLACE_EXISTING);
    }
    
    private void copyBuildFile() throws IOException {
        URL buildFile = m_bundleContext.getBundle().getEntry("templates/build.gradle");
        
        try(InputStream in = buildFile.openStream()) {
            Files.copy(in, m_navigator.getWorkspaceDir().resolve("build.gradle"), StandardCopyOption.REPLACE_EXISTING);
        }
    }

    /**
     * Set a file to be exectued by everyone
     * 
     * @throws IOException
     */
    private void setGradleExecutePermissions(String toDir) throws IOException {
        Path path = new File(toDir + "/bin/gradle").toPath();
        Set<PosixFilePermission> perms = Files.getPosixFilePermissions(path);
        // Now add the execute on top of the normal
        perms.add(PosixFilePermission.GROUP_EXECUTE);
        perms.add(PosixFilePermission.OTHERS_EXECUTE);
        perms.add(PosixFilePermission.OWNER_EXECUTE);
        Files.setPosixFilePermissions(path, perms);
    }

    @Override
    public boolean isProjectInstallAllowed() {
        return false;
    }

    @Override
    public boolean isWorkspaceInstallAllowed() {
        return true;
    }
    
    @Override
    public String getName() {
        return "gradle";
    }
    
    @Command(description = "executes gradle in the workspace if it exits")
    public void gradle(String... args) {
        build(args);
    }
    
    public void build(String... args) {
        ArrayList<String> gradleCmd = new ArrayList<>();
        if (isWindows()) {
            gradleCmd.add("gradlew.bat");
        } else {
            gradleCmd.add("sh");
            gradleCmd.add("gradlew");
        }
        gradleCmd.addAll(Arrays.asList(args));
        startProcess(m_navigator.getWorkspaceDir(), gradleCmd.toArray(new String[0]));
    }

    @Override
    public boolean isInstalled() {
        File workspaceDir = m_navigator.getWorkspaceDir().toFile();
        return new File(workspaceDir.getAbsolutePath() + "/build.gradle").exists();
    }

    private List<String> getGradleVersions() throws IOException {
        URL myUrl = new URL("http://services.gradle.org/distributions/");
        Pattern p = Pattern.compile(".*?<a href=\"/distributions/gradle-(.*?)-bin.zip\">.*?");

        List<String> result = new ArrayList<>();
        try (BufferedReader in = new BufferedReader(new InputStreamReader(myUrl.openStream()));) {
            String line;

            while ((line = in.readLine()) != null) {
                Matcher m = p.matcher(line);
                if (m.find()) {
                    String version = m.group(1);
                    if (isVersionAllowed(version)) {
                        result.add(version);
                    }
                }
            }
        }

        // The website returns the most resent at the top thats not what we want
        Collections.reverse(result);
        return result;
    }

    private boolean isVersionAllowed(String version) {
        for (String a : NOT_FINAL_VERSIONS) {
            if (version.contains(a)) {
                return false;
            }
        }
        return true;
    }

    private void downloadFile(String fromUrl, String to) throws IOException {
        URL website = new URL(fromUrl);
        ReadableByteChannel rbc = Channels.newChannel(website.openStream());
        try (FileOutputStream fos = new FileOutputStream(to)) {
            fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        }
    }

    private void startProcess(Path workingDirectory, String... command) {
        ProcessBuilder pb = new ProcessBuilder(command);
        pb.directory(workingDirectory.toFile());
        // We want the sys err in the out so we can read them both
        pb.redirectErrorStream(true);
        try {
            Process p = pb.start();
            BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                System.out.println(line);
            }
            input.close();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    private boolean isWindows() {
        return (OS.indexOf("win") >= 0);
    }
}
